FROM debian:bookworm
LABEL maintainer="s7b4 <baron.stephane@gmail.com>"

ENV APP_USER=lutim \
	APP_TAG=0.17.0

ENV APP_HOME=/opt/$APP_USER \
	APP_WORK=/home/$APP_USER

# set user/group IDs
RUN groupadd -r "$APP_USER" --gid=999 \
	&& useradd -m -r -g "$APP_USER" --uid=999 "$APP_USER"

# Perl base
RUN apt-get update \
	&& apt-get install --no-install-recommends --yes \
		procps \
		sqlite3 \
		ca-certificates \
		shared-mime-info \
		carton \
		libmodule-install-perl \
		libimage-magick-perl \
		libperl-dev \
		libpq-dev \
		libsqlite3-dev \
		libssl-dev \
		zlib1g-dev \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

RUN apt-get update \
	&& apt-get install --no-install-recommends --yes \
		make \
		gcc \
		curl \
	&& mkdir -vp $APP_HOME $APP_WORK \
	&& curl -sSL "https://framagit.org/fiat-tux/hat-softwares/lutim/-/archive/$APP_TAG/lutim-$APP_TAG.tar.gz" \
		| tar xz --strip-component=1 -C $APP_HOME \
	&& cd $APP_HOME \
	&& carton install \
	&& rm -rf "$APP_HOME/log" "$APP_HOME/t" "$APP_HOME/local/cache" \
	&& rm -rf "$HOME/.cpan"* \
	&& apt-get remove --purge --yes \
		gcc \
		make \
		curl \
	&& apt-get autoremove --purge --yes \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

WORKDIR $APP_HOME

COPY scripts/docker-carton /usr/local/bin/
COPY scripts/entrypoint.sh /entrypoint.sh

# Patch lutim
COPY scripts/*.diff $APP_HOME/
RUN apt-get update \
	&& apt-get install --no-install-recommends --yes \
		patch \
	&& patch --verbose -d $APP_HOME -p1 < $APP_HOME/configurable-tmpdir.diff \
	&& patch --verbose -d $APP_HOME -p1 < $APP_HOME/fix-mojo-user-agent.diff \
	&& apt-get remove --purge patch --yes \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 8080
VOLUME $APP_WORK
